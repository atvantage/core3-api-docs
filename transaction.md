# Fullloop Transaction API

You can use this endpoint to integrate your current system and clone your transactions into the Fullloop system in realtime.

## Prerequisite

- **field_list**: Provided by client,
- **transaction_unique_key_list**: Provided by client,
- **project_id**: Provided by Atvantage,
- **secret_token**: Provided by Atvantage,

## Importing Transactions via API

### Basic Info

**Base Url**: https://api.bizcuitvoc.com/api/core/3

**Endpoint**: /contact

**HTTP Method**: POST

**Authorization Strategy**: Bearer Token

**Payload**: JSON Object


### Payload

Payload should have at least 2 direct keys, *project_id* and *info*. Project id will be provided to you by Atvantage.

Info is an object of object where every sub-object has the following shape,

```json
    {
      "field_value": "some_value",
      "field_type": "String | Number | Date"
    }
```

There should be 1 info object for each field provided in the **field_list**. Each field in the **field_list** needs to have a corresponding object inside the info object where key is the field name and value is a payload object representing the data.

**Example Payload**

```json
    {
      "project_id": "123456789",
      "info": {
        "Location_Meta1": {
          "field_value": "Bangkok",
          "field_type": "String"
        }
      }
    }
```

### Authorization

This endpoint is using the Bearer Token authorization strategy. Every request will be required to pass a secret token in the header. This secret token will be provided by Atvantage.

**Take caution with this token. Do not make this token public**

Every request to this endpoint should add **Authorization** key. The value is a concatenation of the word **Bearer** and the token with a white space in between.

Example: If the token is *foobar*, the authorization value would be **Bearer foobar**.


### Response

**format**: json

You will receive a response with status 200 if your transaction has been accepted with a copy of the transaction stored in the Atvantage system.

If your transaction fails, you receive a response with status 200 and a json object with a key *error*, value will be the error message.


